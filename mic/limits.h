/*  This file is part of the Vc library.

    Copyright (C) 2013 Matthias Kretz <kretz@kde.org>

    Vc is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    Vc is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Vc.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef VC_MIC_LIMITS_H
#define VC_MIC_LIMITS_H
#ifdef CAN_OFFLOAD
#pragma offload_attribute(push, target(mic))
#endif

namespace std
{
// no need for specializations as <Vc/limits> uses broadcasts and they work very efficently on MIC
} // namespace std

#ifdef CAN_OFFLOAD
#pragma offload_attribute(pop)
#endif
#endif // VC_MIC_LIMITS_H
